import { legacy_createStore as createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";

import rootReducer from "./reducers";
import rootSaga from "./saga";

const middlewares = []; // add mỏe middlewares like thunk, redux observable, ...
const sagaMiddleware = createSagaMiddleware(); // Create sagaMiddleware

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware, ...middlewares))
);
sagaMiddleware.run(rootSaga);

export default store;
