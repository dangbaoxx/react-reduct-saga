import { combineReducers } from "redux";
import featureReducer from "../redux/reducers";

const rootReducer = combineReducers({
    //reducer example
    feature: featureReducer
})

export default rootReducer;