import { fork } from "redux-saga/effects";

import featureSagas from "../redux/sagas/index";

export default function* rootSagas() {
  yield fork(featureSagas);
}
