import { all } from "redux-saga/effects";
/**
 * Root [feature]-saga
 */
export default function* sagas() {
  yield all([]);
}
