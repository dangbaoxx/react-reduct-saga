import { get } from "lodash";
import { useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import hobbyAction from "../actions/hobby";

const useDefineHobby = () => {
  const data = useSelector((state) => get(state, "feature.defineHobby"));

  const dispatch = useDispatch();

  const action = useMemo(
    () => bindActionCreators(hobbyAction, dispatch),
    [dispatch]
  );

  return {
    action,
    data,
  };
};

export default useDefineHobby;
