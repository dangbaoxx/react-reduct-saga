import { combineReducers } from "redux";
import defineHobby from "./hobby";

const featureReducer = combineReducers({
  defineHobby,
});

export default featureReducer;
