import { ADD_HOBBY, GET_HOBBY } from "../actions/hobby";

const initialState = {
  isLoading: false,
  hobbyList: [],
};

export default function defineHobby(state = initialState, action) {
  switch (action.type) {
    case ADD_HOBBY:
      const newListHobby = [...state.hobbyList];
      newListHobby.push(action.payload);
      return {
        ...state,
        hobbyList: newListHobby,
      };
    case GET_HOBBY:
      return {
        ...state,
      };
    default:
      return state;
  }
}
