export const ADD_HOBBY = "ADD_HOBBY";
export const GET_HOBBY = "GET_HOBBY";

export function addHobby(payload) {
  return {
    type: ADD_HOBBY,
    payload: payload,
  };
}

export function getHobby(payload) {
  return {
    type: GET_HOBBY,
    payload: payload,
  };
}

export default {
  addHobby,
  getHobby,
};
