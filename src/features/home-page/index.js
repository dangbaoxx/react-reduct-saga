import React, { useEffect } from "react";
import useDefineHobby from "../../redux/hooks/useDefineHobby";
import HobbyList from "./hobby-list/index";
import "./homepage.css";

HomePage.propTypes = {};

function HomePage(props) {
  const {
    data: { hobbyList, isLoading },
    action,
  } = useDefineHobby();

  const handleAddHobby = () => {
    const randomId = 1000 + Math.trunc(Math.random() * 8000);
    const hobby = {
      name: `hobby ${randomId}`,
      id: randomId,
    };
    action.addHobby(hobby);
  };

  return (
    <div>
      <h1>Homepage</h1>
      <h2>Hello</h2>
      {console.log(hobbyList)}
      <button onClick={handleAddHobby}>ADD HOBBY ITEM</button>
      <HobbyList hobbies={hobbyList} />
    </div>
  );
}

export default HomePage;
