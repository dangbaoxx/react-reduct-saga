import React from "react";
import PropTypes from "prop-types";

HobbyList.propTypes = {
  hobbies: PropTypes.object,
};

HobbyList.defaultProps = {
  hobbies: { name: "", id: null },
};

function HobbyList(props) {
  const { hobbies } = props;
  return (
    <div>
      <h3>Hobby List</h3>
      <ul>
        {hobbies.map((hobby) => (
          <>
            <li>{hobby?.name}</li>
          </>
        ))}
      </ul>
    </div>
  );
}

export default HobbyList;
