src
|__ assets
|__ commons
|__ components
|__ features
|__ redux
|   |
|   |__ actions
|   |   |__ action
|   |   |__ ...
|   |
|   |__ hooks
|   |   |__ hook
|   |   |__ ...
|   |
|   |__ reducers
|        |__ reducers
|        |__ ...
|__ routes
|__ store
|   |__index.js
|   |__ reducers.js
